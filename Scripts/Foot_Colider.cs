﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foot_Colider : MonoBehaviour
{
    public Chara_Controler chara;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerStay2D(Collider2D collision)
    {
        chara.state.Hitground();
        Debug.Log("Hit");
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        chara.state.Hitground();
        Debug.Log("Hit");
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (chara.state.GetType().Name != "Jumping")
        {
            chara.state = new Falling(chara);
        }
    }
}
