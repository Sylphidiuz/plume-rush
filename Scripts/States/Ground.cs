﻿using System.Collections;
using System.Collections.Generic;
using States;
using UnityEngine;

public class Ground : State
{
    public Ground(Chara_Controler c) : base(c)
    {
    }

    public override void MoveLeft()
    {
        base.MoveLeft();
    }
    public override void MoveRight()
    {
        base.MoveRight();
    }
    public override void Attack()
    {
        base.Attack();
    }
    public override void Jump()
    {
        base.Jump();
    }
}
