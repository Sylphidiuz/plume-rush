﻿using System.Collections;
using System.Collections.Generic;
using States;
using UnityEngine;

public class Jumping : State
{
    float jump;
    // Start is called before the first frame update
    public Jumping(Chara_Controler c) : base(c)
    {
        jump = chara.jumpspeed * Time.deltaTime;
    }
    public override void Jump()
    {
        if (jump < chara.maxheight)
            chara.transform.position = chara.transform.position + new Vector3(0, chara.jumpspeed * Time.deltaTime, 0);
        else
        {
            
            chara.state = new Falling(chara);
            Debug.Log("State : Falling");
        }
        jump += chara.jumpspeed * Time.deltaTime;
    }
    
}
