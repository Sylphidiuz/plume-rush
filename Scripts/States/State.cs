﻿using System;
using UnityEngine;
using static States.STATES;

namespace States
{
    public abstract class State 
    {
        //Le Controlleur du personnage
        protected Chara_Controler chara;
        //Classe de transformation du personnage. Utilisé notamment dans les translations.
        private Transform tr;
        
        //Constructeur par défaut desactivé, on doit forcément créer un state avec un contexte.
        private State() { }
        
        //Constructeur qui doit être surchargé par les classes filles. 
        public State (Chara_Controler c)
        {
            chara = c;
            tr = chara.transform;
        }
        
        /**
         * Appelé lorsque l'utilisateur appuie sur le bouton "aller à gauche"
         */
        public virtual void MoveLeft() {
            Move(true);
        }
        
        /**
         * Appelé lorsque l'utilisateur appuie sur le bouton "aller à droite"
         */
        public virtual void MoveRight() {
            Move(false);
        }
        
        /**
         * Etat définissant le saut.
         * On applique simplement une translation vers le haut, égale à la vitesse de saut du personnage. 
         */
        public virtual void Jump() {
            tr.position += new Vector3(0, chara.jumpspeed * Time.deltaTime, 0);
            chara.state = new Jumping(chara);
        }
        
        /**
         * TODO
         */
        public virtual void Attack() { }
        
        /**
         * Appelé quand le personnage ne bouge pas (aucun input) 
         */
        public virtual void Idle ()
        {
            chara.animator.SetInteger("state", (int) IDLE);
        }
        
        /**
         * Appelée quand le personnage touche un sol. 
         */
        public virtual void Hitground()
        {
            chara.state = new Ground(chara);
            Debug.Log("State : Ground");
        }

        internal void NotJumping()
        {
            ;
        }
        
        /**
         * Méthode appelée lors d'un mouvement en général.
         * Si on a pas d'objet sur la route, on avance normalement
         * Sinon, on place le personnage sur le point le plus proche de l'obstacle.
         *
         * @param {Boolean} isLeft : Indique si le personnage avance vers la gauche. Si c'est le cas, on applique un
         * modificateur -1 à la vitesse du personage. 
         */
        private void Move(bool isLeft)
        {
            int sens = isLeft ? -1 : 1;
            Collider2D overlapBox =
                Physics2D.OverlapBox(new Vector2(tr.position.x + (sens * chara.speed) * Time.deltaTime, tr.position.y + 1f),
                    new Vector2(0.1f, 0.9f), 0);
            
            if (!overlapBox)
                tr.position += new Vector3(sens * chara.speed * Time.deltaTime, 0, 0);
            else {
                Vector2 cp = overlapBox.ClosestPoint(new Vector2(tr.position.x, tr.position.y));
                tr.position = new Vector3(cp.x - (sens * 0.24f), tr.position.y, tr.position.z);
            }
            chara.animator.SetInteger("state", (int) RUN);
            chara.sprite.flipX = isLeft;
            Debug.Log("move");
        }
    }
}
