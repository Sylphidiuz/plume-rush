﻿using System.Collections;
using System.Collections.Generic;
using States;
using UnityEngine;

public class Chara_Controler : MonoBehaviour
{
    public State state;
    public string stateCheck;
    public bool noKey = true;
    public float speed = 2f;
    public float previousY;
    public float jumpspeed = 0.5f;
    public float maxheight = 3f;
    public Animator animator;
    public SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
         state = new Ground(this);
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        //else if (previousY == transform.position.y) state = new Ground(this);
        stateCheck = state.GetType().Name;
            
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            state.MoveLeft();
            noKey = false;
        } else if (Input.GetKey(KeyCode.RightArrow))
        {
            noKey = false;
            state.MoveRight();
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            noKey = false;
            state.Jump();
        } else if (state.GetType().Name == "Jumping") {
            GetComponent<Rigidbody2D>().isKinematic = false;
            state = new Falling(this);
            Debug.Log("state test");
        }
        if(noKey) state.Idle();
        noKey = true;
        previousY = transform.position.y;
    }
    /*void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 closestPoint = collision.collider.ClosestPoint(new Vector2(transform.position.x, transform.position.y));
        transform.position = new Vector3(closestPoint.x, closestPoint.y, transform.position.z);
        Debug.Log("Mur !");
        Debug.Log(closestPoint);
    }*/


}
